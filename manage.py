#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    if 'test' in sys.argv:
        print('using settings_test.py')
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'webapp.conf.settings_test')
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.conf.settings")

    import django
    django.setup()

    # Override default port for `runserver` command
    from django.core.management.commands.runserver import Command as runserver
    runserver.default_port = "7098"

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
