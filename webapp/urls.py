"""
livspaceit base url config
"""

from django.urls import path, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

urlpatterns = [
    path('django-admin/', admin.site.urls),
    path('api/', include("images.urls")),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('docs/', get_swagger_view(title='LivspaceIt API')),
]
