from django.apps import AppConfig


class AddressConfig(AppConfig):
    """
    images config
    """
    name = 'images'
