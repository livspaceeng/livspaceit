import logging
import os
import tensorflow as tf
from tensorflow.python.keras.backend import set_session

from keras.preprocessing import image
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
import numpy as np

config = tf.ConfigProto(
    device_count={'CPU': 1},
    intra_op_parallelism_threads=1,
    allow_soft_placement=True
)

session = tf.Session(config=config)
set_session(session)
resnet_model = ResNet50(weights='imagenet', pooling='max', include_top=False, input_shape=(224, 224, 3))
graph = tf.get_default_graph()
# resnet_model.predict(np.zeros((1, 224, 224, 3)))


class FeatureExtractor:

    def __init__(self):
        self.input_shape = (224, 224, 3)
        self.weight = 'imagenet'
        self.pooling = 'max'
        # self.resnet_model = ResNet50(weights='imagenet', pooling='max', include_top=False, input_shape=(224, 224, 3))
        # self.resnet_model.predict(np.zeros((1, 224, 224, 3)))

    def get_feature_vector(self, image_path):
        """
        Applies resnet50 model to get the feature vector of an image
        :param image_path:
        :return:
        """
        with graph.as_default():
            set_session(session)
            img = image.load_img(image_path, target_size=(self.input_shape[0], self.input_shape[1]))
            img_data = image.img_to_array(img)
            img_data = np.expand_dims(img_data, axis=0)
            img_data = preprocess_input(img_data)
            feature_vector = resnet_model.predict(img_data)
            norm_feature = feature_vector[0] / np.linalg.norm(feature_vector[0])
            del img
            del img_data
            del feature_vector
            norm_feature = [i.item() for i in norm_feature]
            logging.info("The extracted vector from the image %s is %s" % (image_path, norm_feature))
            return norm_feature
