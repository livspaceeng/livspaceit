# import logging
# import re
# from orb.core.http import HttpClient
#
# from images.models import ImageLinks
# import nltk
#
# from difflib import SequenceMatcher
#
# def similar(a, b):
#     return SequenceMatcher(None, a, b).ratio()
#
# city_map = {
#     "banglore": "bangalore",
#     "bengaluru": "bangalore",
#     "noida": "noida",
#     "gurgaon": "gurgaon",
#     "gurugram": "gurgaon",
#     "ghaziabad": "ghaziabad",
#     "delhi": "delhi",
#     "capital": "delhi",
#     "mumbai": "mumbai",
#     "bombay": "mumbai",
#     "hyderabad": "hyderabad",
#     "chennai": "chennai",
#     "madras": "chennai",
#     "pune": "pune",
#     "singapore": "singapore",
#     "faridabad": "faridabad",
#     "dwarka": "dwarka",
#     "kolkata": "kolkata",
#     "calcutta": "kolkata",
#     "thane": "thane",
#     "navi mumbai": "navi mumbai",
#     "ahemdabad": "ahemdabad",
#     "jaipur": "jaipur",
#     "lucknow": "lucknow",
#     "kochi": "kochi",
#     "surat": "surat"
# }
#
# city_id_mapping = {
#     "bangalore": 1,
#     "delhi": 2,
#     "noida": 3,
#     "gurgaon": 4,
#     "mumbai": 5,
#     "ghaziabad": 6,
#     "faridabad": 7,
#     "other": 8,
#     "dwarka": 9,
#     "hyderabad": 10,
#     "chennai": 12,
#     "pune": 13,
#     "thane": 14,
#     "navi mumbai": 15,
#     "singapore": 16,
#     "kolkata": 17,
#     "jaipur": 18,
#     "lucknow": 19,
#     "ahmedabad": 20,
#     "surat": 21,
#     "kochi": 22
# }
#
# class ProjectLinkService:
#
#     def get_city(self, image_link):
#         cities = city_map.keys()
#         data = image_link.display_name + " " + image_link.data
#         for city in cities:
#             if re.search(city, data, re.IGNORECASE):
#                 return city_map[city]
#         #city not found
#         print("City not found: "+ str(image_link.id))
#
#     def check_cities(self):
#         image_links = ImageLinks.objects.filter(is_deleted=False).all()
#         for image_link in image_links:
#             city = self.get_city(image_link)
#             if city:
#                 print(city)
#
#     def get_customer_details(self):
#         image_links = ImageLinks.objects.filter(is_deleted = False).all()
#         for image_link in image_links:
#             match = None
#             try:
#                 # resp = HttpClient().get(url=image_link.magazine_url)
#                 # # regex = '(Location):(.*)'
#                 regex = '(Interior Designer.*|Design Manager.*|Project Manager.*|Designer.*|Design Associate)|KNW.*:(.*)'
#
#                 match = re.findall(regex, str(image_link.data))
#                 collaborators = ""
#                 for line in match:
#                     collaborators +=  "/n" + str(line)
#                 print(collaborators)
#                 image_link.collaborators = collaborators
#                 image_link.save()
#             except Exception as e:
#                 pass
#             if not match:
#                 print("customer not found:" + str(image_link.id))
#
#     def get_human_names(self, text):
#         tokens = nltk.tokenize.word_tokenize(text)
#         pos = nltk.pos_tag(tokens)
#         sentt = nltk.ne_chunk(pos, binary = False)
#         person_list = []
#         person = []
#         name = ""
#         for subtree in sentt.subtrees(filter=lambda t: t.label() == 'PERSON'):
#             for leaf in subtree.leaves():
#                 person.append(leaf[0])
#
#         return (person)
#
#     def get_customer_names(self):
#         image_links = ImageLinks.objects.filter(is_deleted=False).exclude(magazine_customer_info=None).all()
#         for image_link in image_links:
#             names = self.get_human_names(image_link.collaborators)
#             for name in names:
#                 print(name)
#
#     def add_project_ids(self):
#         image_links = ImageLinks.objects.filter(project_id=0)
#         print(len(image_links))
#         headers = {
#             "Content-Type": "application/json",
#             "X-Client-Id": "Platform-Prod",
#             "X-Client-Secret": "G7j1ZAMzqjo9saxTdg3xVJ8PhjNUxhJW",
#             "X-Requested-By": "1"
#         }
#         count = 0
#         for image_link in image_links:
#             if image_link.magazine_customer_info:
#                 words = image_link.magazine_customer_info.strip().split(" ")
#
#                 if len(words)>2 and words[1] is not "and" or words[1] is not "with":
#                     # name = words[0].strip() + " " + words[1].strip().replace(',', '')
#                     # name = words[0].strip()
#                     name = words[0].strip().replace(',', '')
#                     resp = HttpClient().get(url = "http://api.livspace.com/launchpad-backend/v3/projects?filters=(""status=eq=ACTIVE)%26(stage_id%3Deq%3D10)%26(q=eq="+name+ ")", headers=headers)
#                     projects = resp.json().get("page").get("items")
#                     if projects:
#                         if len(projects)>1:
#                             # var = input("Please enter project_id: ")
#
#                             # image_link.project_id = 0
#                             max_similarity = 0
#                             max_similarity_project_id = None
#                             max_similarity_collabs = None
#                             max_similarity_project_collabs = None
#                             max_similarity_project_name = None
#                             for project in projects:
#                                 if project.get("collaborators") and image_link.collaborators:
#                                     collab_names = str(self.get_human_names(image_link.collaborators))
#                                     collab_string = str(project.get("collaborators"))
#                                     similarity = similar(collab_string, collab_names)
#                                     if similarity and similarity>max_similarity:
#                                         max_similarity = similarity
#                                         max_similarity_project_id = project.get("id")
#                                         max_similarity_collabs = collab_names
#                                         max_similarity_project_collabs = collab_string
#                                         max_similarity_project_name = project.get("display_name")
#
#                             if max_similarity:
#                                 print(max_similarity_project_collabs)
#                                 print(max_similarity_collabs)
#                                 print(max_similarity)
#                                 print(max_similarity_project_name)
#                                 print(image_link.magazine_customer_info)
#                                 var = input("Please accept: ")
#                                 if int(var) > 0:
#                                     print("Accepted")
#                                     image_link.project_id = max_similarity_project_id
#                                     count += 1
#
#                         else:
#                             # image_link.project_id = projects[0]["id"]
#                             # print(projects[0]["id"])
#                             # count += 1
#                             pass
#                         image_link.save()
#         print(count)
#
#
#
#
#
