import logging
import urllib.request

# from orb.core.exceptions import InvalidStateException, InvalidInputException

from webapp.apps.images.clients.milvus import MilvusClient
from webapp.apps.images.services.extract_feature import FeatureExtractor
from webapp.apps.images.services.project_images import ProjectImageVectorService


class SearchEngineService:

    def search_image(self, data):
        """
        Searches for an image in the trained image data
        :param data:
        :return:
        """
        top_designers = None
        image_path = data.get("path")
        city = data.get("city")
        result = urllib.request.urlretrieve(image_path)
        input_vector = FeatureExtractor().get_feature_vector(image_path=result[0])
        logging.info("Customer inspiration image vector: ", input_vector)
        similar_vectors, distance_dict = self.__get_similar_vectors_from_milvus(vector=input_vector)
        del input_vector
        if similar_vectors and distance_dict:
            logging.info("Found similar vectors in milvus")
            project_vectors_dict = self.__fetch_project_vectors(vectors=similar_vectors)
            if not project_vectors_dict:
                return {"message": "Images are not mapped anywhere"}
            top_designers = self.__get_top_designers(topk=3, project_vectors_dict=project_vectors_dict, city=city,
                                                     distance_dict=distance_dict, similar_vectors=similar_vectors)
        if not top_designers:
            return{"message": "No matching designer found"}
        return top_designers

    def __get_similar_vectors_from_milvus(self, vector):
        """
        Calls the milvus search engine to fetch similar vector ids in existing trained data
        :param vector:
        :return:
        """
        client = MilvusClient()
        _, results = client.search_vector(vectors=[vector])
        if not results:
            return None, None
        similar_vectors = []
        distance_dict = {}
        for x in results[0]:
            similar_vectors.append(x.id)
            distance_dict[x.id] = x.distance
        return similar_vectors, distance_dict

    def __fetch_project_vectors(self, vectors):
        """
        Fetch project vectors from trained data
        :param vectors:
        :return:
        """
        project_vector_dict = {}
        project_vectors = ProjectImageVectorService().get_vectors(vectors=vectors)
        for project_vector in project_vectors:
            project_vector_dict[project_vector.get("milvus_vector_id")] = project_vector
        return project_vector_dict

    def __get_top_designers(self, topk, project_vectors_dict, distance_dict, similar_vectors, city):
        """
        Fetches best k matched designers
        :param topk:
        :param project_vectors_dict:
        :param distance_dict:
        :param similar_vectors:
        :param city:
        :return:
        """
        same_city_matches = []
        other_city_matches = []
        same_city_found = 0
        other_city_found = 0
        same_city_designer_set = set()
        other_city_designer_set = set()
        for vector_id in similar_vectors:
            if same_city_found == topk and other_city_found == topk:
                break
            project_vector = project_vectors_dict.get(str(vector_id))
            if same_city_found < topk:
                if city == project_vector.get("city") and project_vector.get("designer_id") not in same_city_designer_set:
                    same_city_found += 1
                    same_city_designer_set.add(project_vector.get("designer_id"))
                    self.__add_designer(match_list=same_city_matches, project_vector=project_vector,
                                        vector_id=vector_id, distance_dict=distance_dict)
            if other_city_found < topk:
                if city != project_vector.get("city") and project_vector.get("designer_id") not in other_city_designer_set:
                    other_city_found += 1
                    other_city_designer_set.add(project_vector.get("designer_id"))
                    self.__add_designer(match_list=other_city_matches, project_vector=project_vector,
                                        vector_id=vector_id, distance_dict=distance_dict)
        return {
            "SAME_CITY": same_city_matches,
            "OTHER_CITITES": other_city_matches
        }

    def __add_designer(self, match_list, project_vector, vector_id, distance_dict):
        """
        Adds the designer info to the given list
        :param match_list:
        :param project_vector:
        :param vector_id:
        :param distance_dict:
        :return:
        """
        match_list.append({
            "project_id": project_vector.get("project_id"),
            "designer_id": project_vector.get("designer_id"),
            "designer_name": project_vector.get("designer_name"),
            "designer_image_url": project_vector.get("designer_image_url"),
            "city": project_vector.get("city"),
            "image_url": project_vector.get("image_url"),
            "magazine_url": project_vector.get("magazine_url"),
            "match_percentage": round((1 - distance_dict.get(vector_id)) * 100)
        })
