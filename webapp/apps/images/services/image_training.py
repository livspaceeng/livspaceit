import logging
import http
import urllib.request
# from orb.core.exceptions import InvalidStateException

from webapp.apps.images.services.extract_feature import FeatureExtractor
from webapp.apps.images.services.project_images import ProjectImageVectorService
from webapp.apps.images.clients.milvus import MilvusClient
from images.models import ProjectImageVector


class ImageTrainingService:

    def extract_and_save_features(self, data):
        """
        To extract feature vector of an image and save the corresponding id
        :param data:
        :return:
        """
        response = None
        image_path = data.get("path")
        extractor = FeatureExtractor()
        vector = self.__extract_image_features(extractor=extractor, path=image_path)
        vector_id = self.__save_vector_in_milvus(vectors=[vector])
        if vector_id:
            response = ProjectImageVectorService().save_project_image_vector(project_id=data.get("project_id"),
                                                                             image_url=image_path, vector_id=vector_id)
        return response

    def bulk_train(self, projects_info):
        """
        Bulk training of data
        :param projects_info:
        :return:
        """
        vectors = []
        extractor = FeatureExtractor()
        for project_info in projects_info:
            image_url = project_info.get("path")
            vector = self.__extract_image_features(extractor=extractor, path=image_url)
            vectors.append(vector)
        vector_ids = self.___bulk_save_vectors_in_milvus(vectors=vectors)
        for index, project_info in enumerate(projects_info):
            image_url = project_info.get("path")
            project_id = project_info.get("project_id")
            vector_id = vector_ids[index]
            ProjectImageVectorService().save_project_image_vector(project_id=project_id, image_url=image_url,
                                                                  vector_id=vector_id)
        return {"message": "SUCCESS"}

    def __extract_image_features(self, extractor, path):
        """
        Extract feature vector from an image
        :param extractor:
        :param path:
        :return:
        """
        return extractor.get_feature_vector(image_path=path)

    def __save_vector_in_milvus(self, vectors):
        """
        Saves the vector in milvus server
        :param vectors:
        :return:
        """
        _, ids = MilvusClient().save_vector(vector_list=vectors)
        if not ids:
            return None
            # InvalidStateException("Not able to save vector in milvus")
        return ids[0]

    def ___bulk_save_vectors_in_milvus(self, vectors):
        """
        Saves the list of vectors in milvus
        :param vectors:
        :return:
        """
        _, ids = MilvusClient().save_vector(vector_list=vectors)
        return ids

    def test_training(self):
        project_images = ProjectImageVector.objects.filter(is_deleted=False, milvus_vector_id__exact='').all()
        extractor = FeatureExtractor()
        for project_image in project_images:
            try:
                logging.info("Starting for: " + str(project_image.id))
                result = urllib.request.urlretrieve(project_image.image_url)
                logging.info("Got image at path: " + result[0])
                vector = self.__extract_image_features(extractor=extractor, path=result[0])
                vector_id = self.__save_vector_in_milvus(vectors=[vector])
                project_image.milvus_vector_id = vector_id
                project_image.save()
                logging.info("Completed for id: " + str(project_image.id))
            except Exception as e:
                logging.info("Got error: " + str(e))
                pass
        return {"message": "SUCCESS"}
