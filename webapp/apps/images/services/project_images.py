# from orb.client.launchpad import LaunchpadClient
# from orb.core.exceptions import InvalidInputException

from images.models import ProjectImageVector
from webapp.apps.images.serializers.project_image import ProjectImageVectorSerializer


class ProjectImageVectorService:

    def save_project_image_vector(self, project_id, image_url, vector_id):
        """
        Maps the project & image to its vector id
        :param project_id:
        :param image_url:
        :param vector_id:
        :return:
        """
        # project = LaunchpadClient().get_project_by_id(project_id=project_id, include=["collaborators", "settings"])
        # designer_id, designer_name = self.__get_designer(project=project)
        data = {
            "project_id": project_id,
            # "designer_id": designer_id,
            # "designer_name": designer_name,
            "image_url": image_url,
            "milvus_vector_id": str(vector_id)
        }
        project_vector = ProjectImageVector(**data)
        project_vector.save()
        return ProjectImageVectorSerializer(instance=project_vector, many=False).data

    def get_vectors(self, vectors):
        """
        Fetches given vectors from the db
        :param vectors:
        :return:
        """
        project_vectors = ProjectImageVector.objects.filter(milvus_vector_id__in=vectors, is_deleted=False)
        return ProjectImageVectorSerializer(instance=project_vectors, many=True).data

    def __get_designer(self, project):
        """
        Get designer information for the project
        :param project:
        :return:
        """
        primary_designer_id = project.get("settings").get("primary_seller_id")
        for collaborator in project.get("collaborators"):
            if collaborator.get("collaborator_id") == primary_designer_id:
                return primary_designer_id, collaborator.get("name")
        return None, None
        # raise InvalidInputException("Designer not present on the project")
