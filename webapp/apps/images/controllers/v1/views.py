from rest_framework.views import APIView
# from orb.core.utils import APIResponse
from rest_framework.response import Response

# from webapp.apps.images.services.image_links import ImageLinkService
# from webapp.apps.images.services.project_link import ProjectLinkService
from webapp.apps.images.services.image_training import ImageTrainingService
from webapp.apps.images.services.search_image import SearchEngineService
from webapp.apps.images.clients.milvus import MilvusClient


# class CreateImageLinks(APIView):
#     """
#     scrape gmail for looks
#     """
#     def post(self, request):
#         """
#
#         :param request:
#         :return:
#         """
#         response = ImageLinkService().create_image_links()
#         return APIResponse.send(response)
#
#
# class CreateProjectLinks(APIView):
#     """
#
#     """
#     def get(self, request):
#         """
#
#         :param request:
#         :return:
#         """
#         response = ProjectLinkService().add_project_ids()
#         return APIResponse.send(response)


class TrainView(APIView):
    """
    Train view to extract vector of images
    """
    def post(self, request):
        """
        To extract feature vector of an image and save the corresponding id
        :param request:
        :return:
        """
        response = ImageTrainingService().test_training()
        return Response(data=response, status=200)

    def patch(self, request):
        """
        Create collection in milvus
        :param request:
        :return:
        """
        table_name = request.data.get("table_name")
        MilvusClient(table_name=table_name).create_table(table_name=table_name)
        return Response(data={"message": "SUCCESS"}, status=200)

    def put(self, request):
        """
        Bulk training of images
        :param request:
        :return:
        """
        response = ImageTrainingService().bulk_train(projects_info=request.data)
        return Response(data=response, status=200)


class SearchEngineView(APIView):
    """
    Image search engine
    """
    def post(self, request):
        """
        To search for similar images in trained images
        :param request:
        :return:
        """
        response = SearchEngineService().search_image(data=request.data)
        return Response(data=response, status=200)
