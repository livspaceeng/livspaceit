# Loading json file
import json
import pathlib

path = pathlib.Path(__file__).parent / '../resources/test_data.json'
test_file = open(str(path))
TEST_DATA = json.load(test_file)
test_file.close()
