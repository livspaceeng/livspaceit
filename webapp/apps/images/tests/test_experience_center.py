import copy
from django.test import TestCase
from django.test.client import Client

from .data import TEST_DATA

test_data = TEST_DATA


# class FolderServiceTest(TestCase):
#     """
#     Wakanda Test cases class
#     """
#
#     def setUp(self):
#         """To set up input data and expected output response"""
#         self.client = Client()
#         self.default_experience_center_data = copy.deepcopy(test_data.get('experience_center'))
#         self.default_ec_constant = copy.deepcopy(test_data.get('ec_constant'))
#
#     def test_get_ecs(self):
#         """To get Experience centers"""
#         ECService().add_ec(data=self.default_experience_center_data)
#         response = self.client.get('/api/v1/experience-centers')
#         self.assertEqual(response.status_code, 200)
#         ecs = response.json()["response"].get("results")
#         self.assertEqual(len(ecs), 1)
#         self.assertEqual(ecs[0].get("display_name"), self.default_experience_center_data.get("display_name"))
#
#     def test_get_ec_constants(self):
#         """To get Experience center constants"""
#         ECConstantsService().add_ec_constant(data=self.default_ec_constant)
#         response = self.client.get('/api/v1/ec-constants')
#         self.assertEqual(response.status_code, 200)
#         ec_constants = response.json()["response"].get("results")
#         self.assertEqual(len(ec_constants), 1)
#         self.assertEqual(ec_constants[0].get("name"), self.default_ec_constant.get("name"))
