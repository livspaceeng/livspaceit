import os

# from webapp.apps.images.services.image_training import ImageTrainingService


def get_imlist(path):
    return [os.path.join(path, f) for f in os.listdir(path) if (f.endswith('.jpeg') or f.endswith('.jpg') or f.endswith('.png'))]


project_ids = [670748, 667429, 666871, 666167, 663322, 662951, 662949, 662031, 658432, 658050, 654245, 652231, 649799,
               649729, 648036, 645914, 644883, 644742, 643952, 641919, 641830, 641068, 639238, 635591, 632686, 631907,
               631083, 631075, 631009, 628203, 628102, 627652, 627053, 626836, 626736]

data = []
image_directory = "/Users/vinamra/Downloads/Alok"
img_list = get_imlist(path=image_directory)
for i, img_path in enumerate(img_list):
    data.append({
        "project_id": project_ids[i],
        "path": img_path
    })
print(data)
# ImageTrainingService().bulk_train(projects_info=data)
