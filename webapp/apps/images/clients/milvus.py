import logging

from milvus import Milvus, DataType, MetricType, IndexType

from webapp.apps.images.common.constants import VECTOR_DIMENSION, COLLECTION_NAME


class MilvusClient:

    collection_name = None
    client = None
    is_connected = False

    def __init__(self, table_name=None):
        self.collection_name = table_name if table_name else COLLECTION_NAME

    def connect(self):
        self.client = Milvus(host='18.219.123.214', port='19530')
        self.is_connected = True

    def create_table(self, table_name):
        if not self.is_connected:
            self.connect()
        table_param = {
            'collection_name': table_name if table_name else COLLECTION_NAME,
            'dimension': VECTOR_DIMENSION,
            'index_file_size': 1024,
            'metric_type': MetricType.L2
        }
        self.client.create_collection(table_param)
        param = {'nlist': 16384}
        self.client.create_index(self.collection_name, IndexType.IVF_FLAT, param)

    def drop_table(self):
        if not self.is_connected:
            self.connect()
        self.client.drop_collection(collection_name=self.collection_name)

    def save_vector(self, vector_list):
        if not self.is_connected:
            self.connect()
        status, ids = self.client.insert(collection_name=self.collection_name, records=vector_list)
        return status, ids

    def search_vector(self, vectors):
        if not self.is_connected:
            self.connect()
        search_param = {'nprobe': 16}
        status, res = self.client.search(collection_name=self.collection_name, query_records=vectors, top_k=100,
                                         params=search_param)
        return status, res
