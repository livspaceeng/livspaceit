from django.urls import path

from .controllers.v1 import views as v1_views

urlpatterns = [
    # path('v1/create-image-links', v1_views.CreateImageLinks.as_view(), name='iml'),
    # path('v1/create-project-links', v1_views.CreateProjectLinks.as_view(), name='iml'),

    # Train API to extract vectors out of an image
    path('v1/train', v1_views.TrainView.as_view(), name='train-image'),
    # Bulk train of images
    path('v1/train-bulk', v1_views.TrainView.as_view(), name='bulk-train-images'),
    # Image Search Engine to search for similar images
    path('v1/search', v1_views.SearchEngineView.as_view(), name='search-image'),
    # Create collection in milvus
    path('v1/create-table', v1_views.TrainView.as_view(), name='create-table')
]
