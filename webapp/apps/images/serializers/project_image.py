from datetime import datetime

from rest_framework import serializers
# from orb.core.serializers import SerializedDateTimeField

from images.models import ProjectImageVector


class ProjectImageVectorSerializer(serializers.ModelSerializer):

    # created_at = SerializedDateTimeField(default=datetime.strptime("9999-12-31 00:00:00", "%Y-%m-%d %H:%M:%S"))
    # modified_at = SerializedDateTimeField(default=datetime.strptime("9999-12-31 00:00:00", "%Y-%m-%d %H:%M:%S"))

    class Meta:
        model = ProjectImageVector
        exclude = ['is_deleted']
