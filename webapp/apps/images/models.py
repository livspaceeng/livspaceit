from django.db import models


class BaseModel(models.Model):
    """
        This is a abstract model class to add is_deleted, created_at and modified at fields in any model
    """
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        """ Soft delete """
        self.is_deleted = True
        self.save()


class ImageLinks(BaseModel):
    """
    Image meta
    """
    display_name = models.CharField(max_length=255)
    magazine_url = models.CharField(max_length=500)
    drive_url = models.CharField(max_length=500)
    data = models.TextField(default=None)
    magazine_customer_info = models.TextField(null=True)
    project_id = models.IntegerField(null=True)
    collaborators = models.TextField(null=True)
    location = models.TextField(null=True)

    class Meta:
        managed = True
        db_table = 'image_links'


class ImageProjectLinks(BaseModel):
    """
    Project meta
    """
    image_link_id = models.IntegerField()
    project_id = models.IntegerField()
    city_id = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'project_links'


class ProjectImageVector(BaseModel):
    """
    Saves the project id, image url of project and the corresponding milvus vector id of the image
    """
    project_id = models.IntegerField(db_index=True)
    designer_id = models.IntegerField(null=True)
    designer_name = models.CharField(max_length=100, null=True)
    designer_image_url = models.CharField(max_length=500, null=True)
    city = models.CharField(max_length=100, null=True)
    image_url = models.CharField(max_length=500)
    magazine_url = models.CharField(max_length=500, null=True)
    milvus_vector_id = models.CharField(max_length=100, db_index=True)

    class Meta:
        db_table = "project_image_vectors"
