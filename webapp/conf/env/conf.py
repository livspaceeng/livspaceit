CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "127.0.0.1",
        "USERNAME": "postgres",
        "PASSWORD": "livspace",
        "DB_NAME": "livspace_it",
        "PORT": "5432",
    },
    "EVENT": {
        "ENV": "local"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "AXLE": {
        "HOST": "http://axle.alpha.livspace.com/",
        "CLIENT_ID": "Platform-Stage",
        "CLIENT_SECRET": "q12qKlQeVkPZZnuXnhXkZe0rMjn5ERXD"
    },
    "ENV": "local",
    "APM": {
        "INDEX_NAME": "livspace-it-local"
    }
}

GATEWAY = {
    "HOST": "api.alpha.livspace.com",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "DAAKIYA": {
        "HOST": "daakiya.alpha.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    },
    "LAUNCHPAD": {
        "HOST": "launchpad-backend.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False
        }
    }
}
